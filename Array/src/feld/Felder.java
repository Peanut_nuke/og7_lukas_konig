package feld;

public class Felder {

	 //unsere Zahlenliste zum Ausprobieren
	  private int[] zahlenliste = {5,8,4,3,9,1,2,7,6,0};
	  
	  //Konstruktor
	  public Felder(){}

	  //Methode die Sie implementieren sollen
	  //ersetzen Sie den Befehl return 0 durch return ihre_Variable
	  
	  //die Methode soll die gr��te Zahl der Liste zur�ckgeben
	  public int maxElement(){
		  int max = zahlenliste[0];
	      for (int i = 1; i < zahlenliste.length; i++) {
	         if (zahlenliste[i] > max) max = zahlenliste[i];
	      }
	      System.out.println("Max is " + max);
		  return max;
	  }

	  //die Methode soll die kleinste Zahl der Liste zur�ckgeben
	  public int minElement(){
		  int min = zahlenliste[0];
	      for (int i = 1; i < zahlenliste.length; i++) {
	         if (zahlenliste[i] < min) min = zahlenliste[i];
	      }
	      System.out.println("Min is " + min);
	    return min;
	  }
	  
	  //die Methode soll den abgerundeten Durchschnitt aller Zahlen zur�ckgeben
	  public int durchschnitt(){
      int durchschnitt =0;
      int min = zahlenliste[0];
      for (int i = 0; i < zahlenliste.length; i++){
      	 durchschnitt = durchschnitt +zahlenliste[0]; 
      }
      durchschnitt = durchschnitt / zahlenliste.length; 
	    return durchschnitt;
	  }

	  //die Methode soll die Anzahl der Elemente zur�ckgeben
	  //der Befehl zahlenliste.length; k�nnte hierbei hilfreich sein
	  public int anzahlElemente(){
	    return zahlenliste.length;
	  }

	  //die Methode soll die Liste ausgeben
	  public String toString(){
		  for (int i = 0; i < zahlenliste.length; i++) {
				zahlenliste[i] = i;
	         System.out.println(i);
			}
	
		
	    return null;
	  }

	  //die Methode soll einen booleschen Wert zur�ckgeben, ob der Parameter in
	  //dem Feld vorhanden ist
	  public boolean istElement(int zahl){
	    return false;
	  }
	  
	  //die Methode soll das erste Vorkommen der
	  //als Parameter �bergebenen  Zahl liefern oder -1 bei nicht vorhanden
	  public int getErstePosition(int zahl){
	    return 0;
	  }
	  
	  //die Methode soll die Liste aufsteigend sortieren
	  //googlen sie mal nach Array.sort() ;)
	  public void sortiere(){

	  }

	  public static void main(String[] args) {
	    Felder testenMeinerLoesung = new Felder();
	    System.out.println(testenMeinerLoesung.maxElement());
	    System.out.println(testenMeinerLoesung.minElement());
	    System.out.println(testenMeinerLoesung.durchschnitt());
	    System.out.println(testenMeinerLoesung.anzahlElemente());
	    System.out.println(testenMeinerLoesung.toString());
	    System.out.println(testenMeinerLoesung.istElement(9));
	    System.out.println(testenMeinerLoesung.getErstePosition(5));
	    testenMeinerLoesung.sortiere();
	    System.out.println(testenMeinerLoesung.toString());
	  }
	}
