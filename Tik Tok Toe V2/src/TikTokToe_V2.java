import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TikTokToe_V2 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TikTokToe_V2 frame = new TikTokToe_V2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TikTokToe_V2() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(3, 3, 0, 0));
		
		
for(int i=0;i<9;i++) {
	JButton btn = new JButton(" ");
	btn.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		change(btn);
		}

		private void change(JButton btn) {
			if(btn.getText().equals("")) {
				btn.setText("X");
			}else if (btn.getText().equals("X")) {
				btn.setText("O");
			}else {
				btn.setText("");
				
			}
			
		}
	});
	contentPane.add(btn);
}
}
}
