package bubble;

public class Bubblesort {
	public static int[] bubblesort(int[] zusortieren) {
		int temp;
		for(int i=1; i<zusortieren.length; i++) {
			for(int j=0; j<zusortieren.length-i; j++) {
				if(zusortieren[j]>zusortieren[j+1]) {
					temp=zusortieren[j];
					zusortieren[j]=zusortieren[j+1];
					zusortieren[j+1]=temp;
				}
				return zusortieren;
			}
		}
	
	}
	public static void main(String[] args) {
		
		int[] durcheinander={1,5,3,6,4,45};
		int[] sortiert=bubblesort(durcheinander);
		
		for (int i = 0; i<sortiert.length; i++) {
			System.out.print(sortiert[i] + ", ");
		}
	
	}
}
