package ms4;

import ms4.Buch;

public class Buch implements Comparable<Buch> {
	private String autor;
	private String titel;
	private String isbn;
	
	public Buch(String autor, String titel, String isbn) {
		this.autor = autor;
		this.titel = titel;
		this.isbn = isbn;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	
	@Override
	public String toString() {
		return ("autor:" + autor  + "isbn:" + isbn + "titel:"+ titel) ;
	}

	@Override
	public boolean equals(Object o) {
		return true;
	}

	@Override
	public int compareTo(Buch aBuch) {
		return this.getIsbn().compareTo(aBuch.getIsbn()) ;
	}
}
