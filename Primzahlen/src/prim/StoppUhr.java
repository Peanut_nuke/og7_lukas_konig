package prim;

public class StoppUhr {
private long start ;
private long stopp ;

public void start() {
	this.start = System.currentTimeMillis();
}
public void stopp() {
	this.stopp = System.currentTimeMillis()-this.start; 
}
public void reset() {	
    this.start = 0;
    this.stopp = 0;
}
public long getZeit() {
	return this.stopp;
}
}