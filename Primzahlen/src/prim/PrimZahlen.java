package prim;

public class PrimZahlen {

public static boolean einPrim(long n) {
	
	boolean primZahl = true;
	long i = 2;
	
	if (n <= 1) {
		return primZahl= false;
	}
	
	while(i<n) {
		if(n%i==0) {
			primZahl=false;
		}
		i++;
	}
	return primZahl;
}

public static void main(String[]argv) {
System.out.println(einPrim(1));
}
}